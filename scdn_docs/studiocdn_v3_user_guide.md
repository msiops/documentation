# <u>The StudioCDN User Guide</u>



![main_image](scdn_docs/user_guide_images/main_image.png)

#### <u>About StudioCDN</u>

StudioCDN is a watermarked secure content delivery web-platform capable of sending any number, size, and type of files to anyone in the world with an email address. 

StudioCDN supports a wide range of secure file-sharing use cases that allow senders to intuitively create and deliver standard one-to-one / one-to-many deliveries to full scale branded marketing and D2C distributions with security, confidence, and drag & drop ease. 



------



![login](/Users/michaelgamble/Documents/Typora/user_guide_images/login.png)

#### <u>Authentication</u> 

Login can be achieved through entering your registered username and password (optional MFA) or your enterprise `SSO`.  credentials at [app.studiocdn.com](StudioCDN.com).  



------



![packages_list](/Users/michaelgamble/Documents/Typora/user_guide_images/packages_list.png)

#### <u>Packages Lists</u>

Clicking on `Packages` will take you here. The StudioCDN packages list can be filterd by clicking one of the following methods.

- `All`: Default view, displays all packages in chronological order
- `Active`: Displays currently active packages in chronological order 
- `Scheduled`: Displays currently scheduled pacakges in chronological order
- `Draft`: Displays current drafts in chronological order
- `Expired`: Displays expired packages in chronological order 



------



![contacts_page](/Users/michaelgamble/Documents/Typora/user_guide_images/contacts_page.png)

#### <u>Contacts</u>

Clicking on `Contacts` will take you here. StudioCDN supports creating and storing lists of contacts for use in sending StudioCDN deliveries. Contacts may be added manually or imported in `.csv` format.

- **Creating a contact list**: Start by clicking on `Create Group`, then `Add Contact` and enter your contact's `name` and `email address`. Click `Add` to add another contact, or `Save` to save and exit.

- **Importing a contacts list**:  StudioCDN supports importing contacts lists in `.csv` format. Contacts list must have exactly one column for `name` and exactly one column for `email`. 

  To import a contact list, start by clicking on `Create Group` or select an existing contact list, then `Import List`, next locate your contacts list on your local drive and finally click `open`. 

- **CSV Formatting / CSV Template**: Please see the image above for reference in creating a CSV list, alternatively a CSV template can be download [here](https://studiocdn.com/wp-content/uploads/2018/04/StudioCDN-CSV-List-Template-UPDATE.csv).



------



![inbox](/Users/michaelgamble/Documents/Typora/user_guide_images/inbox.png)

#### <u>Inbox</u>

Clicking on `Inbox` will take you here. StudioCDN's inbox feature allows creating disposable inboxes so senders may receive secure content deliveries from non-StudioCDN users direct to their email. 

- **Creating an inbox**: Start by entering a name for your inbox and optionally a company, next click `Create Link`, finally click `Copy Link` to copy the inbox link to your clipboard. 

  Once copied you have the option to paste into a personal email, social account, etc..., clicking the `Remove` icon will block further access to inbox deliveries. 



------



![main_image](/Users/michaelgamble/Library/Mobile Documents/com~apple~Preview/Documents/studiocdn_v3_user_guide/main_image.png)

#### <u>Package Editor</u>

Clicking on `New Package` take you here get you here. StudioCDN's package editor contains a rich text and advanced editor along with robust content settings to compose and send `HTML` emails. 



------



<img src="/Users/michaelgamble/Documents/Typora/user_guide_images/content_settings.png" alt="content_settings" style="zoom:50%;" />

#### <u>Content Settings</u>

Modifying any of the following can impact the security of your delivery to a varying degree. For reference we suggest keeping `Watermark` and `Link Lock` enabled and `Download` <u>disabled</u> when sending sensitive materials.

- `Watermark`: When enabled StudioCDN's watermarking engine will apply session based, forensic audio watermarks to each audio and video* asset contained in the package.
- `Download`: When disabled the package will be sent `Stream Only`,  audio and video files will not be available for download by the recipients. When dsiabled recipients will have the option to stream or download the assets. Both stream and downloaded files carry the StudioCDN forensic watermark.
- `Forward`: When enabled, recipients will have the option to forward a copy of the package using the landing page forwarding field. Supported files will be 're-watermarked' to the new recipient. 
- `Link Lock`: When enabled, recipients may only acces the package landing page from a single device and browser. 
- `Schedule`: Use this feature to schedule a delivery up to 90 days in advance. 
- `Expires`: Adjust this setting to allow access for 1-60 days.

*Video watermarking supports most .mp4 and .mov files



------



<img src="/Users/michaelgamble/Documents/Typora/user_guide_images/landing_page_details.png" alt="landing_page_details" style="zoom:50%;" />

#### <u>Landing Page Detail</u>

Each item in this section will be displaed to recipients on the landing page.

- `Image`
- `Artist Name`
- `Project Title` 



------



<img src="/Users/michaelgamble/Documents/Typora/user_guide_images/image-20210925125918396.png" alt="image-20210925125918396" style="zoom:50%;" />

#### <u>Assets Panel</u>

Assets can be added from your local drive, popular cloud services like Box and OneDrive, as well as from [locker.studiocdn.com](). 

- **Uploading Assets**: Clicking on the `+` button will open your file browser. Navigate to select your files and click `open`. Alternatively you can drag and drop your assets to the assets panel.

- Cloud**Connect**: Currently supports uploading files directly from `OneDrive`, `Dropbox`, and `Box` to StudioCDN. 

  After clicking on the `CloudConnect` option, select the cloud service you would like to load files from, and follow the prompts onscreen to login, select, and upload your files. 

- **Locker**: StudioCDN Locker is a watermarked, live-folder secure file delivery service by MSI. StudioCDN supports Instant 'cross-loading' of files between the platforms. 

  After clicking on the `Locker` option, select the `Assets` or `Workgroup` heading, and next the folder, then files you would like to 'cross-load'.



------



![to_section](/Users/michaelgamble/Documents/Typora/user_guide_images/to_section.png)

#### <u>Contacts Section</u>

The contacts sections is composed of two fields, `To` and an optional `From`.  

- `To` field: Click into this field to add a recipient to your email delivery. Contacts can be added through, type, copy & paste, or by attaching a contacts list. 
- `From` field: *Optional*; this field is displayed when you have been added as proxy sender to another user. Click into this field to select the account you would like to send as.



------



![main_image](/Users/michaelgamble/Library/Mobile Documents/com~apple~Preview/Documents/studiocdn_v3_user_guide/main_image.png)

#### <u>Email Editor</u>

StudioCDN is equipped with `Standard` and `Advanced` email editors. The `Standard` email editor contains basic email editing functionalities found in most all desktop and web email editors. The `Advanced` editor is contains advanced, no code HTML email building functionalities found in many popular email marketing tools.

- **Standard Editor**: Enabled by default on all new accounts. Use this editor to create and send simple, rich text email deliveries with watermarked assets.
- **Advanced Editor**: Toggling this option enables the `Advanced Editor`. Use this editor to create profesional email marketing deliveries with watermarked assets.



------



![package_details](/Users/michaelgamble/Documents/Typora/user_guide_images/package_details.png)

#### <u>Package Details</u>

Clicking on any `Active` or `Expired` package will take you here. The packages details page provides analytics and actions for packatges after they have been sent.

- `Clone`: Clicking this option creates an exact copy of the parent delivery which may be edited and sent.
- `Resend`: Clicking this option immediately sends an exact copy of the parent delivery to the recipient. 
- `Recall / Recall All`: Clicking `Recall` for a specific recipient revokes further access to the package content for that particular recipient. Clicking `Recall All` revokes further access to the package content for <u>all</u> recipients. 
- `Forwarding off`: Clicking `Forwarding Off` revokes forwarding rights from all recipients.
- `Export Activity`: Clicking `Export Activity` initiates a download of a `.csv` file containing user activity for the specific package. 



------



![settings](/Users/michaelgamble/Documents/Typora/user_guide_images/settings.png)

#### <u>Settings</u>

Clicking on the settings icon will take you here. The settings page provides areas to specify default settings which are applied to all new pacakges.

- `Subject`: Implement a default subject line to be applied to all new pacakges.
- `Body`: Impement a default body to be applied to all new pacakges.
- `Signature`: Implement a personalized signature to be applied to all new packages.
- Content settings: Specify default content settings for `Download`, `Forwarding`, and `Link Lock`. Select a perfered `Expiration` and  `Email Editor`.
- `MFA`: Modify, enable or dsiable `MFA` settungs. 



------



![landing_page](/Users/michaelgamble/Documents/Typora/user_guide_images/landing_page.png)

#### <u>Landing Page</u>

Once a delivery is complete, each recipient will receive an email containing a unique url to access the content. Recipients must accept the 'Copyright and Link Protection' acknowledgement to stream or download the content. The StudioCDN landing page supports stream, single and multi-file, and download to `Box` options.

- `Download`: Click the download icon next to any asset, or tick one or more assets and then click the `Download` button to download multiple files simultaneously. 
- `Send to Box`: Click the send to box icon next to any asset, or tick one or more assets and then click the `Send to Box` button to login and download the seleted files to your `Box` account.
- `STREAM`: When audio and video files are marked as `STREAM`, these files may only be downloaded.  
- `Content Locked`: Displayed upon loading, this message is presented when a package has been 'locked' to another device and / or browser. As a resolution, users should return to the original device and browser and try again.









